import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel roulette = new RouletteWheel();
        Scanner reader = new Scanner(System.in);
        int userMoney = 1000;

        while(true){
            System.out.println("Your Current Balance Is: $" + userMoney);
            System.out.println("Would you like to bet? (y/n)");

            String respond = reader.next().toLowerCase();

            if(respond.equals("n")){
                System.out.println("You have: $" + userMoney);
                System.out.println("Have A Great Day :).");
                break;
            }

            if(!respond.equals("y")){
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
                continue;
            }

            System.out.println("Enter The Amount You Would Like To Bet: $");
            int betAmount = reader.nextInt();

            while(betAmount > userMoney){
                System.out.println("You dont have ecough money :( ");
                System.out.println("Please Re-Enter the Amount: $");
                betAmount = reader.nextInt();
            }

            System.out.print("Enter the number you want to bet on (0-36): ");
            int betNumber = reader.nextInt();
            
            while(betNumber <= 0 || betNumber > 36) {
                System.out.println("Invalid bet number. Please choose a number between 0 and 36.");
                System.out.println("Please Re-Enter the Number: ");
                betNumber = reader.nextInt();
            }
            userMoney -= betAmount;
            roulette.spin();
            int result = roulette.getNumber();

            if(betNumber == result){
                int winning = betAmount * 35;
                userMoney += winning;
                System.out.println("The number was: " + result);
                System.out.println("Congratulations! You won $" + winning + " You Have: $" + userMoney);
            }else{
                System.out.println("The number was: " + result);
                System.out.println("unfortunately You Have Lost: $" + betAmount + " You Have: $" + userMoney);
            }
        }
    }
}
